/********************************************************
Dermatac Targeting code
Author: Dinu Pillai
Last updated: 01/20/2022 (update territory alignment to 2022 by taking out alignment from base table and using sand_mdm table here)
Updated logic for OSI
Description: Step 3 - Code for AWT and OSI account ranking
*********************************************************/

drop table if exists sand_cast.dermatac_rpt_awt_base;
create table sand_cast.dermatac_rpt_awt_base as
Select a.*,
tot_d_kit_post_launch/tot_kit as d_kit_utilization,
sum(tot_d_kit_post_launch) over (partition by territory_code, facility_caregiver) /
sum(tot_kit) over (partition by territory_code, facility_caregiver) d_kit_utilization_fac,
sum(tot_kit) over (partition by territory_code, facility_caregiver) tot_kits_ord,
sum(tot_kit) over (partition by territory_code) tot_kits_terr,
sum(tot_d_kit_post_launch) over (partition by territory_code, facility_caregiver) tot_d_kits_fac
from
(
select 
/*region,
district,
territory_code,
awt_territory_code,
atr_territory_code,
osi_territory_code,
isr_territory_code,
tsr_territory_code,
inside_sales_flag,
salesrep_name,
awt_salesrep_name,
atr_salesrep_name,
osi_salesrep_name,
isr_salesrep_name,
tsr_salesrep_name,
rvp_name,
dm_name,*/



a.territory_oracle as territory_code,
a.rep_pin as rep_upin,
concat(upper(a.rep_last_name),', ',upper(a.rep_first_name)) as salesrep_name,
a.region_id_oracle as district,
a.rsm_pin as rsm_upin,
concat(upper(a.rsm_last_name),', ',upper(a.rsm_first_name)) as dm_name,
a.area_id_oracle as region,
a.rsl_pin as rsl_upin,
concat(upper(a.rsl_last_name),', ',upper(a.rsl_first_name)) as rsl_name,
a.territory_type,
shipto_market_category,
shipto_market_segment,
shipto_market_segment_desc,
caregiver_site_use_id as site_use_id,
facility_number,
facility_caregiver,
facility_caregiver_address,
facility_caregiver_city,
a.state,
facility_caregiver_zip,
facility_caregiver_phone_number,
caregiver_type,
physician_name,
ro_npi,
ro_doctor_phone,
order_status,
initial_resupply_order_type,
placement_type,
market_category_type,

sum(case when initial_resupply_order_type = 'Initial Order' and placement_type ='Organic' and sku in ('DTGF10PKS','DTGF05PKS','DTGF10PKM','DTGF05PKM','DTGF10PKL','DTGF05PKL') then 1 else 0 end) as inital_organic_d_kit,
sum(case when initial_resupply_order_type = 'Initial Order' and placement_type ='Organic' and sku in ('M8275051/10','M8275051/5','M8275052/10','M8275052/5', 'M8275053/10', 'M8275053/5','M8275065/5') then 1 else 0 end) as inital_organic_m_kit,
sum(case when initial_resupply_order_type = 'Resupply Order' and placement_type ='Organic' and sku in ('DTGF10PKS','DTGF05PKS','DTGF10PKM','DTGF05PKM','DTGF10PKL','DTGF05PKL') then 1 else 0 end) as Resupply_organic_d_kit,
sum(case when initial_resupply_order_type = 'Resupply Order' and placement_type ='Organic' and sku in ('M8275051/10','M8275051/5','M8275052/10','M8275052/5', 'M8275053/10', 'M8275053/5','M8275065/5') then 1 else 0 end) as Resupply_organic_m_kit,
sum(case when placement_type ='Organic'  then 1 else 0 end) as tot_organic_kit,
sum(case when initial_resupply_order_type = 'Resupply Order' and placement_type ='Post Acute Transitions' and sku in ('DTGF10PKS','DTGF05PKS','DTGF10PKM','DTGF05PKM','DTGF10PKL','DTGF05PKL') then 1 else 0 end) as Resupply_transition_d_kit,
sum(case when initial_resupply_order_type = 'Resupply Order' and placement_type ='Post Acute Transitions' and sku in ('M8275051/10','M8275051/5','M8275052/10','M8275052/5', 'M8275053/10', 'M8275053/5','M8275065/5') then 1 else 0 end) as Resupply_transition_m_kit,
sum(case when placement_type ='Post Acute Transitions'  then 1 else 0 end) as tot_transition_kit,
--count(ro_number) as total_kit,
sum(case when  placement_type ='Organic' or (placement_type = 'Post Acute Transitions' and initial_resupply_order_type = 'Resupply Order') then 1 else 0 end) as tot_kit,
sum(case when sku in ('DTGF10PKS','DTGF05PKS','DTGF10PKM','DTGF05PKM','DTGF10PKL','DTGF05PKL') then 1 else 0 end ) as tot_d_kit_post_launch
--case when sku in ('M8275051/10','M8275051/5','M8275052/10','M8275052/5', 'M8275053/10', 'M8275053/5','M8275065/5') then 1 and 0
--count()
from sand_cast.dermatac_targeting_base_layer b
left join sand_mdm.FIELD_SALES_TERR_ALIGN_2022 a on a.zip_code_text = b.facility_caregiver_zip and a.rep_type_jca_in_oracle = 'AWT'
where b.territory_type='AWT'
and trim(caregiver_type) in ('HA','WCC')
and order_status in ('CLOSED','PLACED')
and ro_status_code in ('OPN','CLO')
and roa_ship_date is not null
group by 1,2,3,4,5,6,7,8,9,10,
11,12,13,14,15,16,17,18,19,20,
21,22,23,24,25,26,27,28,29)a;
compute stats sand_cast.dermatac_rpt_awt_base;
--Inserted 162541 row(s)
--Inserted 152123 row(s)
--Inserted 148852 row(s)


--AWT facility kit distribtion
drop table if exists sand_cast.dermatac_facility_kit_dist;
create table sand_cast.dermatac_facility_kit_dist as
Select region,
district,
territory_code,
facility_number,
facility_caregiver,
tot_kits_ord,
running_total_kits,
tot_kits_terr,
running_total_kits/tot_kits_terr kits_dist,
f_rank
from
(
select region,
district,
territory_code,
facility_number,
facility_caregiver,
tot_kits_ord,
sum(tot_kits_ord) over (partition by territory_code order by tot_kits_ord desc rows between unbounded preceding and current row) as running_total_kits,
sum(tot_kits_ord) over (partition by territory_code) tot_kits_terr,
row_number() over (partition by territory_code order by tot_kits_ord desc) f_rank

from(
select distinct  region,
district,
territory_code,
facility_number,
facility_caregiver,
tot_kits_ord
from sand_cast.dermatac_rpt_awt_base) rb
)rb1;
compute stats sand_cast.dermatac_facility_kit_dist;
--Inserted 20546 row(s)
--Inserted 11857 row(s)

--Create an initial layer

drop table if exists sand_cast.dermatac_facility_init_layer;
create table sand_cast.dermatac_facility_init_layer as
select 
 territory_code
 ,rep_upin
 ,salesrep_name
 ,district
 ,rsm_upin
 ,dm_name
 ,region
 ,rsl_upin
 ,rsl_name
 ,territory_type

/*,region
,district
,territory_code
,awt_territory_code
,atr_territory_code
,osi_territory_code
,isr_territory_code
,tsr_territory_code
,inside_sales_flag
,salesrep_name
,awt_salesrep_name
,atr_salesrep_name
,osi_salesrep_name
,isr_salesrep_name
,tsr_salesrep_name
,rvp_name
,dm_name
,territory_type*/
,site_use_id
,facility_number
,facility_caregiver
,facility_caregiver_address
,facility_caregiver_city
,state
,facility_caregiver_zip
,facility_caregiver_phone_number
,caregiver_type
,physician_name
,ro_npi
,ro_doctor_phone
,sum(tot_kit) dressings_ord_phy
,max(tot_kits_ord) dressings_ord_fac
from sand_cast.dermatac_rpt_awt_base b
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22;
compute stats sand_cast.dermatac_facility_init_layer;
--Inserted 94358 row(s)
--Inserted 85937 row(s)

drop table if exists sand_cast.dermatac_awt_list_all;
create table sand_cast.dermatac_awt_list_all as
select b.*,x.sap_id, f.kits_dist, f.f_rank
from sand_cast.dermatac_facility_init_layer b
inner join sand_cast.dermatac_facility_kit_dist f on f.facility_number=b.facility_number 
                                                and f.territory_code=b.territory_code
left join reporting_mdm.sap_xref x on x.extid = b.site_use_id and x.idtyp ='MSD01'
where  dressings_ord_phy <>0;
compute stats sand_cast.dermatac_awt_list_all;
--Inserted 94358 row(s)
--Inserted 85877 row(s)



/*AWT target list old*/
/*
drop table if exists sand_cast.dermatac_target_list_awt;
create table sand_cast.dermatac_target_list_awt as
select b.*,x.sap_id, f.kits_dist, f.f_rank
from sand_cast.dermatac_facility_init_layer b
inner join sand_cast.dermatac_facility_kit_dist f on f.facility_number=b.facility_number 
                                                and f.territory_code=b.territory_code
left join reporting_mdm.sap_xref x on x.extid = b.site_use_id and x.idtyp ='MSD01'
Where f.kits_dist<=0.8 and f.f_rank<=30;
compute stats sand_cast.dermatac_target_list_awt;
*/

drop table if exists sand_cast.dermatac_target_list_awt_fac;
create table sand_cast.dermatac_target_list_awt_fac as
select 
 b.region
,b.rsl_upin
,b.rsl_name
,b.district
,b.rsm_upin
,b.dm_name
,b.territory_code
,b.rep_upin
,b.salesrep_name
,b.territory_type
,x.sap_id
,b.site_use_id
,b.facility_number
,b.facility_caregiver
,b.facility_caregiver_address
,b.facility_caregiver_city
,b.state as facility_caregiver_state
,b.facility_caregiver_zip
,b.facility_caregiver_phone_number
,b.caregiver_type
,b.dressings_ord_fac
,f.kits_dist
,f.f_rank
,5000 as annual_opp_value

from sand_cast.dermatac_facility_init_layer b
inner join sand_cast.dermatac_facility_kit_dist f on f.facility_number=b.facility_number 
                                                and f.territory_code=b.territory_code
left join reporting_mdm.sap_xref x on x.extid = b.site_use_id and x.idtyp ='MSD01'
Where f.kits_dist<=0.8 and f.f_rank<=30
--and b.territory_code ='AWT2316'
 
 union

select 
 b.region
,b.rsl_upin
,b.rsl_name
,b.district
,b.rsm_upin
,b.dm_name
,b.territory_code
,b.rep_upin
,b.salesrep_name
,b.territory_type
,x.sap_id
,b.site_use_id
,b.facility_number
,b.facility_caregiver
,b.facility_caregiver_address
,b.facility_caregiver_city
,b.state as facility_caregiver_state
,b.facility_caregiver_zip
,b.facility_caregiver_phone_number
,b.caregiver_type
,b.dressings_ord_fac
,f.kits_dist
,f.f_rank
,5000 as annual_opp_value

from sand_cast.dermatac_facility_init_layer b
inner join sand_cast.dermatac_facility_kit_dist f on f.facility_number=b.facility_number 
                                                and f.territory_code=b.territory_code
left join reporting_mdm.sap_xref x on x.extid = b.site_use_id and x.idtyp ='MSD01'
Where f.f_rank<=10
--and b.territory_code ='AWT2316';
compute stats sand_cast.dermatac_target_list_awt_fac;
--Inserted 3506 row(s)


drop table if exists sand_cast.dermatac_target_list_awt_phy;
create table sand_cast.dermatac_target_list_awt_phy as
select 
 b.region
,b.rsl_upin
,b.rsl_name
,b.district
,b.rsm_upin
,b.dm_name
,b.territory_code
,b.rep_upin
,b.salesrep_name
,b.territory_type
,x.sap_id
,b.site_use_id
,b.facility_number
,b.facility_caregiver
,b.facility_caregiver_address
,b.facility_caregiver_city
,b.state as facility_caregiver_state
,b.facility_caregiver_zip
,b.facility_caregiver_phone_number
,b.caregiver_type
,b.physician_name	
,b.ro_npi	
,b.ro_doctor_phone
,b.dressings_ord_phy
,b.dressings_ord_fac
,f.kits_dist
,f.f_rank

from sand_cast.dermatac_facility_init_layer b
inner join sand_cast.dermatac_facility_kit_dist f on f.facility_number=b.facility_number 
                                                and f.territory_code=b.territory_code
left join reporting_mdm.sap_xref x on x.extid = b.site_use_id and x.idtyp ='MSD01'
Where f.kits_dist<=0.8 and f.f_rank<=30
--and b.territory_code ='AWT2316'
 
 union

select 
 b.region
,b.rsl_upin
,b.rsl_name
,b.district
,b.rsm_upin
,b.dm_name
,b.territory_code
,b.rep_upin
,b.salesrep_name
,b.territory_type
,x.sap_id
,b.site_use_id
,b.facility_number
,b.facility_caregiver
,b.facility_caregiver_address
,b.facility_caregiver_city
,b.state as facility_caregiver_state
,b.facility_caregiver_zip
,b.facility_caregiver_phone_number
,b.caregiver_type
,b.physician_name	
,b.ro_npi	
,b.ro_doctor_phone
,b.dressings_ord_phy
,b.dressings_ord_fac
,f.kits_dist
,f.f_rank

from sand_cast.dermatac_facility_init_layer b
inner join sand_cast.dermatac_facility_kit_dist f on f.facility_number=b.facility_number 
                                                and f.territory_code=b.territory_code
left join reporting_mdm.sap_xref x on x.extid = b.site_use_id and x.idtyp ='MSD01'
Where f.f_rank<=10
--and b.territory_code ='AWT2316';
compute stats sand_cast.dermatac_target_list_awt_phy;
--61216
--Inserted 58313 row(s)
/*

select * from sand_cast.dermatac_target_list_awt_fac
select * from sand_cast.dermatac_target_list_awt_phy

select count(1) from sand_cast.dermatac_target_list_awt

select count (distinct facility_number) from sand_cast.dermatac_awt_list_all --11802

80% 
select count(distinct facility_number) from 
(
select b.*,x.sap_id, f.kits_dist, f.f_rank
from sand_cast.dermatac_facility_init_layer b
inner join sand_cast.dermatac_facility_kit_dist f on f.facility_number=b.facility_number 
                                                and f.territory_code=b.territory_code
left join reporting_mdm.sap_xref x on x.extid = b.site_use_id and x.idtyp ='MSD01'
Where f.kits_dist<=0.8 
)a
--3383
select count (distinct facility_number) from sand_cast.dermatac_target_list_awt ----3364

select * from sand_cast.dermatac_target_list_awt
where facility_number ='3290275'
where territory_code ='AWT0131'

--Inserted 147301 row(s)

select count (distinct facility_number)
from (
select b.*,x.sap_id, f.kits_dist, f.f_rank
from sand_cast.dermatac_facility_init_layer b
inner join sand_cast.dermatac_facility_kit_dist f on f.facility_number=b.facility_number 
                                                and f.territory_code=b.territory_code
left join reporting_mdm.sap_xref x on x.extid = b.site_use_id and x.idtyp ='MSD01'
)a*/



/*Dermatac OSI base*/
drop table sand_cast.dermatac_rpt_osi_base;
create table sand_cast.dermatac_rpt_osi_base as
Select a.*,
tot_d_kit_post_launch/tot_kit as d_kit_utilization,
sum(tot_d_kit_post_launch) over (partition by territory_code, facility_caregiver) /
sum(tot_kit) over (partition by territory_code, facility_caregiver) d_kit_utilization_fac,
sum(tot_kit) over (partition by territory_code, facility_caregiver) tot_kits_ord,
sum(tot_d_kit_post_launch) over (partition by territory_code, facility_caregiver) tot_d_kits_fac
from
(
select 
/*
region,
district,
territory_code,
awt_territory_code,
atr_territory_code,
osi_territory_code,
isr_territory_code,
tsr_territory_code,
inside_sales_flag,
salesrep_name,
awt_salesrep_name,
atr_salesrep_name,
osi_salesrep_name,
isr_salesrep_name,
tsr_salesrep_name,
rvp_name,
dm_name,
*/
a.territory_oracle as territory_code,
a.rep_pin as rep_upin,
concat(upper(a.rep_last_name),', ',upper(a.rep_first_name)) as salesrep_name,
a.region_id_oracle as district,
a.rsm_pin as rsm_upin,
concat(upper(a.rsm_last_name),', ',upper(a.rsm_first_name)) as dm_name,
a.area_id_oracle as region,
a.isl_pin as rsl_upin,
concat(upper(a.isl_last_name),', ',upper(a.isl_first_name)) as rsl_name,
a.territory_type,
shipto_market_category,
shipto_market_segment,
shipto_market_segment_desc,
caregiver_site_use_id as site_use_id,
facility_number,
facility_caregiver,
facility_caregiver_address,
facility_caregiver_city,
a.state,
facility_caregiver_zip,
facility_caregiver_phone_number,
caregiver_type,
physician_name,
ro_npi,
ro_doctor_phone,
order_status,
initial_resupply_order_type,
--sku,
--case when sku in ('M8275051/10','M8275051/5','M8275052/10',
--'M8275052/5', 'M8275053/10', 'M8275053/5','M8275065/5') then 'M'
--when sku in ('DTGF10PKS','DTGF05PKS','DTGF10PKM','DTGF05PKM','DTGF10PKL','DTGF05PKL') then 'D' else 'NA' end as sku_flag,
placement_type,
market_category_type,

sum(case when initial_resupply_order_type = 'Initial Order' and placement_type ='Organic' and sku in ('DTGF10PKS','DTGF05PKS','DTGF10PKM','DTGF05PKM','DTGF10PKL','DTGF05PKL') then 1 else 0 end) as inital_organic_d_kit,
sum(case when initial_resupply_order_type = 'Initial Order' and placement_type ='Organic' and sku in ('M8275051/10','M8275051/5','M8275052/10','M8275052/5', 'M8275053/10', 'M8275053/5','M8275065/5') then 1 else 0 end) as inital_organic_m_kit,
sum(case when initial_resupply_order_type = 'Resupply Order' and placement_type ='Organic' and sku in ('DTGF10PKS','DTGF05PKS','DTGF10PKM','DTGF05PKM','DTGF10PKL','DTGF05PKL') then 1 else 0 end) as Resupply_organic_d_kit,
sum(case when initial_resupply_order_type = 'Resupply Order' and placement_type ='Organic' and sku in ('M8275051/10','M8275051/5','M8275052/10','M8275052/5', 'M8275053/10', 'M8275053/5','M8275065/5') then 1 else 0 end) as Resupply_organic_m_kit,
sum(case when placement_type ='Organic'  then 1 else 0 end) as tot_organic_kit,
sum(case when initial_resupply_order_type = 'Resupply Order' and placement_type ='Post Acute Transitions' and sku in ('DTGF10PKS','DTGF05PKS','DTGF10PKM','DTGF05PKM','DTGF10PKL','DTGF05PKL') then 1 else 0 end) as Resupply_transition_d_kit,
sum(case when initial_resupply_order_type = 'Resupply Order' and placement_type ='Post Acute Transitions' and sku in ('M8275051/10','M8275051/5','M8275052/10','M8275052/5', 'M8275053/10', 'M8275053/5','M8275065/5') then 1 else 0 end) as Resupply_transition_m_kit,
sum(case when placement_type ='Post Acute Transitions'  then 1 else 0 end) as tot_transition_kit,
--count(ro_number) as total_kit,
sum(case when  placement_type ='Organic' or (placement_type = 'Post Acute Transitions' and initial_resupply_order_type = 'Resupply Order') then 1 else 0 end) as tot_kit,
sum(case when sku in ('DTGF10PKS','DTGF05PKS','DTGF10PKM','DTGF05PKM','DTGF10PKL','DTGF05PKL') then 1 else 0 end ) as tot_d_kit_post_launch
--case when sku in ('M8275051/10','M8275051/5','M8275052/10','M8275052/5', 'M8275053/10', 'M8275053/5','M8275065/5') then 1 and 0
--count()
from sand_cast.dermatac_targeting_base_layer b
left join sand_mdm.INSIDE_SALES_TERR_ALIGN_2022 a on a.zip_code_text = b.facility_caregiver_zip and a.rep_type_jca_in_oracle = 'OSI'
where b.territory_type='OSI'
and trim(caregiver_type) in ('HA','WCC')
and order_status in ('CLOSED','PLACED')
and ro_status_code in ('OPN','CLO')
and facility_number not in (select distinct facility_number from sand_cast.dermatac_target_list_awt_fac)
and roa_ship_date is not null
group by 1,2,3,4,5,6,7,8,9,10,
11,12,13,14,15,16,17,18,19,20,
21,22,23,24,25,26,27,28,29)a;
compute stats sand_cast.dermatac_rpt_osi_base;
--Inserted 41604 row(s)

select * from sand_mdm.INSIDE_SALES_TERR_ALIGN_2022 where territory_code is not null

drop table if exists sand_cast.dermatac_facility_osi_rank;
create table sand_cast.dermatac_facility_osi_rank as
select region,
district,
territory_code,
facility_number,
facility_caregiver,
tot_kits_ord,
row_number() over (partition by territory_code order by tot_kits_ord desc) f_rank

from(
select distinct  region,
district,
territory_code,
facility_number,
facility_caregiver,
tot_kits_ord
from sand_cast.dermatac_rpt_osi_base) rb;
compute stats sand_cast.dermatac_facility_osi_rank;
--Inserted 8351 row(s)



drop table if exists sand_cast.dermatac_facility_osi_init_layer;
create table sand_cast.dermatac_facility_osi_init_layer as
select 
 territory_code
 ,rep_upin
 ,salesrep_name
 ,district
 ,rsm_upin
 ,dm_name
 ,region
 ,rsl_upin
 ,rsl_name
 ,territory_type

/*,region
,district
,territory_code
,awt_territory_code
,atr_territory_code
,osi_territory_code
,isr_territory_code
,tsr_territory_code
,inside_sales_flag
,salesrep_name
,awt_salesrep_name
,atr_salesrep_name
,osi_salesrep_name
,isr_salesrep_name
,tsr_salesrep_name
,rvp_name
,dm_name
,territory_type*/
,site_use_id
,facility_number
,facility_caregiver
,facility_caregiver_address
,facility_caregiver_city
,state
,facility_caregiver_zip
,facility_caregiver_phone_number
,caregiver_type
,physician_name
,ro_npi
,ro_doctor_phone
,sum(tot_kit) dressings_ord_phy
,max(tot_kits_ord) dressings_ord_fac
from sand_cast.dermatac_rpt_osi_base b
where territory_code is not null
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22;
compute stats sand_cast.dermatac_facility_osi_init_layer;
--Inserted 26269 row(s)

--OSI target list fac

drop table if exists sand_cast.dermatac_target_list_osi_fac;
create table sand_cast.dermatac_target_list_osi_fac as
select  
distinct b.region
,b.rsl_upin
,b.rsl_name
,b.district
,b.rsm_upin
,b.dm_name
,b.territory_code
,b.rep_upin
,b.salesrep_name
,b.territory_type
,x.sap_id
,b.site_use_id
,b.facility_number
,b.facility_caregiver
,b.facility_caregiver_address
,b.facility_caregiver_city
,b.state as facility_caregiver_state
,b.facility_caregiver_zip
,b.facility_caregiver_phone_number
,b.caregiver_type
--,b.physician_name	
--,b.ro_npi	
--,b.ro_doctor_phone
--,b.dressings_ord_phy
,b.dressings_ord_fac
from sand_cast.dermatac_facility_osi_init_layer b
inner join sand_cast.dermatac_facility_osi_rank f on f.facility_number=b.facility_number 
                                                and f.territory_code=b.territory_code
left join reporting_mdm.sap_xref x on x.extid = b.site_use_id and x.idtyp ='MSD01'
where f.f_rank <=50
and b.dressings_ord_phy <>0 ;
compute stats sand_cast.dermatac_target_list_osi_fac;
--Inserted 400 row(s)

select * from sand_cast.dermatac_target_list_osi_phy

--OSI target list phy

drop table if exists sand_cast.dermatac_target_list_osi_phy;
create table sand_cast.dermatac_target_list_osi_phy as
select  
distinct b.region
,b.rsl_upin
,b.rsl_name
,b.district
,b.rsm_upin
,b.dm_name
,b.territory_code
,b.rep_upin
,b.salesrep_name
,b.territory_type
,x.sap_id
,b.site_use_id
,b.facility_number
,b.facility_caregiver
,b.facility_caregiver_address
,b.facility_caregiver_city
,b.state as facility_caregiver_state
,b.facility_caregiver_zip
,b.facility_caregiver_phone_number
,b.caregiver_type
,b.physician_name	
,b.ro_npi	
,b.ro_doctor_phone
,b.dressings_ord_phy
,b.dressings_ord_fac
from sand_cast.dermatac_facility_osi_init_layer b
inner join sand_cast.dermatac_facility_osi_rank f on f.facility_number=b.facility_number 
                                                and f.territory_code=b.territory_code
left join reporting_mdm.sap_xref x on x.extid = b.site_use_id and x.idtyp ='MSD01'
where f.f_rank <=50
and b.dressings_ord_phy <>0 ;
compute stats sand_cast.dermatac_target_list_osi_phy;
--Inserted 3582 row(s)


/*
drop table if exists sand_cast.dermatac_target_list_osi;
create table sand_cast.dermatac_target_list_osi as
select b.*, m.sap_id
from sand_cast.dermatac_top50 b
left join sand_cast.oracle_sap_mapping m on b.facility_number = m.customer_account_number and sap_id not in ('Unable To Map','Pending Mapping');
compute stats sand_cast.dermatac_target_list_osi;
--Inserted 6768 row(s)
*/
